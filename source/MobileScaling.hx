package;

import flash.Lib;
import flixel.math.FlxRect;


/**
 * ...
 * @author Owyoow
 *
**/
class MobileScaling 
{
    private static inline var AT_1_X_MAX_WIDTH = 1000;

    public static var scale(default, null):Float = 1;

    public static var suffix(default, null):String = "";

    public static function getGameSize (designWidth:Float, designHeight:Float, isLandscape:Bool):FlxRect
    {
        var gameRect:FlxRect = FlxRect.get();
        var deviceRect:FlxRect = FlxRect.get(0, 0, Math.min(Lib.current.stage.stageWidth, Lib.current.stage.stageHeight),
                                            Math.max(Lib.current.stage.stageWidth, Lib.current.stage.stageHeight));

        if (deviceRect.width > AT_1_X_MAX_WIDTH)
        {
            scale = 2;
            suffix = "@2x";
        }

        var defaultAspect:Float = designWidth / designHeight;
        var deviceAspect:Float = deviceRect.width / deviceRect.height;

        if (deviceAspect > defaultAspect)
        {
            gameRect.height = designHeight;
            gameRect.width = Math.ceil((gameRect.height / 2.0) * 2);
        }
        else
        {
            gameRect.width = designWidth;
            gameRect.height = Math.ceil((gameRect.width / 2.0) * 2);
        }

        if (isLandscape)
        {
            var temp:Float = gameRect.width;
            gameRect.width = gameRect.height;
            gameRect.height = temp;
        }

        return gameRect;
    }
}