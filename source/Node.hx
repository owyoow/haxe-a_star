package;

import de.polygonal.ds.Heapable;

class Node implements Heapable<Node>
{
    public var parent:Node = null;
    public var position:Int;
    public var value:Int = 0;
    public var cost:Int = 0;

    public var x(default, null):Int;
    public var y(default, null):Int;
    public var weight(default, null):Int;
    public var tileIndex(default, null):Int;

    public function new (x:Int, y:Int, tileIndex:Int, weight:Int = 1)
    {
        this.x = x;
        this.y = y;
        this.tileIndex = tileIndex;
        this.weight = weight;
    }

    public function compare(other:Node):Int
    {
        return value - other.value;
    }

    public function equalsXY(other:Node):Bool
    {
        return this.x == other.x && this.y == other.y;
    }
}