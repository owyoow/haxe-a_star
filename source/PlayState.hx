package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.math.FlxPoint;
import flixel.tile.FlxTilemap;
import flixel.addons.plugin.FlxMouseControl;
import flixel.addons.display.FlxExtendedSprite;

class PlayState extends FlxState
{
    private static inline var TILESIZE:Int = 32;

    private var _start:FlxExtendedSprite;
    private var _target:FlxExtendedSprite;

    private var _map:FlxTilemap;

    private var _levelArray:Array<Array<Int>>;

    private var _path:Array<IntPoint>;

    private var _pathfinding:Pathfinding;

	override public function create ():Void
	{
		super.create();

        FlxG.plugins.add(new FlxMouseControl());

        _levelArray = new Array();
        _path = new Array();

        for (y in 0...15)
        {
            var row:Array<Int> = new Array();
            for( x in 0...20)
            {
                if (x == 0 || x == 19 || y == 0 || y == 14)
                {
                    row.push(2);
                }
                else if( x == 6 && y > 0 && y < 12)
                {
                    row.push(2);
                }
                else if(x == 12 && y > 2 && y < 14)
                {
                    row.push(2);
                }
                else
                {
                    row.push(0);
                }
            }
            _levelArray.push(row);
        }

        _map = new FlxTilemap();
        _map.loadMapFrom2DArray(_levelArray, "assets/images/tiles.png", TILESIZE, TILESIZE, OFF, 0, 0);
        add(_map);

        _start = new FlxExtendedSprite(4 * TILESIZE, 4 * TILESIZE, "assets/images/start.png");
        _start.enableMouseDrag(true);
        _start.enableMouseSnap(TILESIZE, TILESIZE, false, true);
        _start.mouseStopDragCallback = stopDrag;
        add(_start);

        _target = new FlxExtendedSprite(14 * TILESIZE, 10 * TILESIZE, "assets/images/target.png");
        _target.enableMouseDrag(true);
        _target.enableMouseSnap(TILESIZE, TILESIZE, false, true);
        _target.mouseStopDragCallback = stopDrag;
        add(_target);

        var walkableTiles:Array<Int> = new Array();
        walkableTiles.push(0);
        _pathfinding = new Pathfinding(_levelArray, walkableTiles);

        var staticAvoid1:FlxExtendedSprite = new FlxExtendedSprite(8 * TILESIZE, 8 * TILESIZE, "assets/images/target.png");
        staticAvoid1.enableMouseDrag(true);
        staticAvoid1.enableMouseSnap(TILESIZE, TILESIZE, false, true);
        staticAvoid1.mouseStartDragCallback = staticStartDrag;
        staticAvoid1.mouseStopDragCallback = staticStopDrag;
        add(staticAvoid1);
        _pathfinding.addAvoidPointAt(8, 8);

        updatePath();
	}

	override public function update (elapsed:Float):Void
	{
        if(FlxG.mouse.justPressed)
        {
            var mousePos:IntPoint = toGrid(FlxG.mouse.getPosition());
            var startPos:IntPoint = toGrid(_start.getPosition());
            var targetPos:IntPoint = toGrid(_target.getPosition());
            if(!mousePos.equals(startPos) && !mousePos.equals(targetPos))
            {
                if(_map.getTile(mousePos.x, mousePos.y) == 0 || _map.getTile(mousePos.x, mousePos.y) == 1)
                {
                    _map.setTile(mousePos.x, mousePos.y, 2);
                    _levelArray[mousePos.y][mousePos.x] = 2;
                }
                else
                {
                    _map.setTile(mousePos.x, mousePos.y, 0);
                    _levelArray[mousePos.y][mousePos.x] = 0;
                }
                _pathfinding.updateGrid(_levelArray);
                updatePath();
            }
        }
    
		super.update(elapsed);
	}

    private function staticStartDrag (obj:FlxExtendedSprite, x:Int, y:Int):Void
    {
        var pos:IntPoint = toGrid(obj.getPosition());
        _pathfinding.removeAvoidPointAt(pos.x, pos.y);
    }

    private function staticStopDrag (obj:FlxExtendedSprite, x:Int, y:Int):Void
    {
        var pos:IntPoint = toGrid(obj.getPosition());
        _pathfinding.addAvoidPointAt(pos.x, pos.y);
        updatePath();
    }

    private function toGrid (position:FlxPoint):IntPoint
    {
        return new IntPoint(Math.floor(position.x / TILESIZE), Math.floor(position.y / TILESIZE));
    }

    private function updatePath ():Void
    {
        for(p in _path)
        {
            if(_map.getTile(p.x, p.y) != 2)
            {
                _map.setTile(p.x, p.y, 0);
            }
        }

        _path = _pathfinding.findPath(toGrid(_start.getPosition()), toGrid(_target.getPosition()));
        for(p in _path)
        {
            _map.setTile(p.x, p.y, 1);
        }
    }

    private function stopDrag (obj:FlxExtendedSprite, x:Int, y:Int):Void
    {
        updatePath();
    }
}
