package;

class IntPoint 
{
    public var x:Int;
    public var y:Int;

    public function new (x:Int = 0, y:Int = 0)
    {
        this.x = x;
        this.y = y;
    }

    public function equals (point:IntPoint):Bool
    {
        return x == point.x && y == point.y;
    }
}