package;

import flixel.FlxGame;
import flixel.math.FlxRect;
import openfl.display.Sprite;

class Main extends Sprite
{
	public function new ()
	{
		super();

        //var gameRect:FlxRect = MobileScaling.getGameSize(640, 960, false);
		//addChild(new FlxGame(Std.int(gameRect.width), Std.int(gameRect.height), PlayState));
        addChild(new FlxGame(640, 480, PlayState));
	}
}