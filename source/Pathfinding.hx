package;

import flixel.FlxG;
import de.polygonal.ds.Heap;

class Pathfinding 
{
    private var _queue:Heap<Node>;

    private var _walkableTiles:Array<Int>;

    private var _grid:Array<Array<Node>>;
    private var _pointsToAvoid:Array<Array<Int>>;

    private var _directions:Array<IntPoint> = [new IntPoint(-1, 0), new IntPoint(1, 0), new IntPoint(0, 1), new IntPoint(0, -1)];

    public function new (grid:Array<Array<Int>>, walkableTiles:Array<Int>)
    {
        _pointsToAvoid = new Array();
        for(y in 0...grid.length)
        {
            var row:Array<Int> = new Array();
            for(x in 0...grid[0].length)
            {
                row.push(0);
            }
            _pointsToAvoid.push(row);
        }
        _walkableTiles = walkableTiles;
        updateGrid(grid);
    }

    public function updateGrid (grid:Array<Array<Int>>):Void
    {
        _grid = new Array();
        for (y in 0...grid.length)
        {
            var row:Array<Node> = new Array();
            for (x in 0...grid[0].length)
            {
                row.push(new Node(x, y, grid[y][x]));
            }
            _grid.push(row);
        }
    }

    public function findPath (start:IntPoint, goal:IntPoint, grid:Array<Array<Int>> = null):Array<IntPoint>
    {
        var path:Array<IntPoint> = new Array();

        if(_walkableTiles.indexOf(_grid[start.y][start.x].tileIndex) == -1 || _walkableTiles.indexOf(_grid[goal.y][goal.x].tileIndex) == -1)
        {
            return path;
        }

        for(y in 0..._grid.length)
        {
            for(x in 0..._grid[0].length)
            {
                _grid[y][x].parent = null;
                _grid[y][x].value = 0;
                _grid[y][x].cost = 0;
            }
        }

        var startNode:Node = _grid[start.y][start.x];
        var goalNode:Node = _grid[goal.y][goal.x];

        _queue = new Heap();
        _queue.add(startNode);

        while(_queue.size() > 0)
        {
            var currentNode:Node = _queue.bottom();
            _queue.remove(_queue.bottom());
            for(neighbor in getNeighbors(currentNode))
            {
                var newCost:Int = currentNode.cost + neighbor.weight;
                if((neighbor.parent == null || newCost < neighbor.cost))
                {
                    neighbor.cost = newCost;
                    neighbor.value = newCost + heuristics(goalNode, neighbor);
                    neighbor.parent = currentNode;
                    _queue.add(neighbor);
                }
            }

            if(currentNode.equalsXY(goalNode))
            {
                break;
            }
        }

        path = reconstructPath(startNode, goalNode);

        return path;
    }

    public function addAvoidPointAt (x:Int, y:Int):Void
    {
        _pointsToAvoid[y][x] = 1;
    }

    public function removeAvoidPointAt (x:Int, y:Int):Void
    {
        _pointsToAvoid[y][x] = 1;
    }

    public function removeAllAvoidPoints ():Void
    {
        for(y in 0..._pointsToAvoid.length)
        {
            for(x in 0..._pointsToAvoid[0].length)
            {
                _pointsToAvoid[y][x] = 0;
            }
        }
    }

    private function reconstructPath (start:Node, goal:Node):Array<IntPoint>
    {
        var p:Array<IntPoint> = new Array();
        var current:Node = goal;
        p.push(new IntPoint(current.x, current.y));
        while(!current.equalsXY(start))
        {
            current = current.parent;
            if(current != null)
            {
                p.push(new IntPoint(current.x, current.y));
            }
            else
            {
                
                break;
            }
        }

        return p;
    }

    private function getNeighbors (node:Node):Array<Node>
    {
        var neighbors:Array<Node> = new Array();

        for(dir in _directions)
        {
            var pos:IntPoint = new IntPoint(node.x + dir.x, node.y + dir.y);

            if(pos.x >= 0 && pos.x < _grid[0].length && pos.y >= 0 && pos.y < _grid.length)
            {
                if(_grid[pos.y][pos.x].value == 0 && _walkableTiles.indexOf(_grid[pos.y][pos.x].tileIndex) != -1 && _pointsToAvoid[pos.y][pos.x] == 0)
                {   
                    neighbors.push(_grid[pos.y][pos.x]);
                }
            }
        }

        return neighbors;
    }

    private function heuristics (cur:Node, target:Node):Int
    {
        return Std.int(Math.abs(cur.x - target.x) + Math.abs(cur.y - target.y));
    }
}